var resources = {
    rum: {
        score: 1
    },

    coin: {
        score: 1
    },

    gold: {
        score: 3
    }
};

var Resource = function (resourceType) {
    this.type = resourceType;
    this.score = resources[this.type].score;
};


exports.resource = Resource;






