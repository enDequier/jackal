var mapModule = require('../map/map').map;
var RoomController = require('../map/roomController/room-controller').RoomController;
var EventEmitter = require('events').EventEmitter;
var Room = function (id, collectorEmitter) {
    var that = this;
    this.emitter = new EventEmitter();
    this.is = 'room';
    this.users = {};
    this.usersCount = 0;
    this.leader = false;
    this.id = id;
    this.href = '/room?id=' + id;
    this.name = 'room' + id;
    this.mapModule = new mapModule;
    this.roomController = new RoomController(this.mapModule);
    this.open = true;
    this.turn = null;

    this.addUserInRoom = function (user) {
        if (this.usersCount === 0) {
            this.leader = user.id;
            user.leader = true;
        }
        this.users[user.id] = user;
        this.usersCount += 1;
        this.addRoomEventsToUserSocket(user);


    };

    this.closeRoom = function () {
        this.open = false;
    };

    this.changeLeader = function () {
        if (this.usersCount > 0) {
            var users = this.users,
                first;
            for (var key in users) {
                if (users.hasOwnProperty(key)) {
                    first = users[key];
                    break;
                }
            }
            this.leader = first.id;
            first.leader = true;
            this.emitter.emit('room:leaderHasBeenChanged', this.leader);
        }
        else {
            collectorEmitter.emit('room:isEmpty', this.id);
        }
    };

    this.setTurnLine = function () {
        var users = this.users,
            turnLine = [];
        for (var key in users) {
            turnLine.push(users[key].id)
        }
        this.turnLine = turnLine;
    };

    this.checkUserInRoom = function (userId) {
        return this.users.hasOwnProperty(userId);
    };

    this.getNextUserInTurnLine = function (currentUserId) {
        if (this.turnLine) {
            var i,
                turnLine = this.turnLine,
                turnLineLength = turnLine.length,
                next = null;

            for (i = 0; i < turnLineLength; i += 1) {
                if (turnLine[i] == currentUserId) {
                    next = (turnLine.hasOwnProperty((i + 1).toString()))? turnLine[i + 1] : turnLine[0];
                    break;
                }
            }

            return next;
        }
    };

    this.changeTurn = function () {
        if (this.turn === null) {
            this.turn = this.turnLine[0];
        } else {
            this.turn = this.getNextUserInTurnLine(this.turn);

        }
        that.emitToAllInRoom('turn', this.turn);
    };

    this.removeUserFromRoom = function (user) {
        delete this.users[user.id];
        this.usersCount -= 1;
        user.setDefaults();
        if (this.leader == user.id)  {
            this.changeLeader();
        }
        if (!this.open) {
            if (this.turn = user.id) {
                this.changeTurn(user.id);
            }
            this.setTurnLine();
        }
        this.emitter.emit('room:userLeft', user);
    };

    this.join = function (user) {
        if (user) {
            this.emitter.emit('room:newUserJoined', user);
            var socket = user.getSocket();
            this.addUserInRoom(user);
            var usersInRoom = this.getUsersListWithoutThisUser(user);

            if (usersInRoom.length > 0) {
                socket.emit('usersInRoom', usersInRoom);
            }
            var you = user.getPublicData();
            you.you = true;
            socket.emit('newUsersJoin', you);
            this.users[this.leader].getSocket().emit('notReady');
        }
    };

    this.setAllUsersRoomControllerEvents = function () {
        var users = this.users;
        for (var key in users) {
            this.roomController.setEventsToUser(users[key], this);
        }
    };


    this.startGame = function (user) {
        if (this.checkReadyList() && user.id == this.leader && this.open) {
            this.closeRoom();
            this.setTurnLine();
            this.changeTurn();
            this.mapModule.setUsers(this.getUsersList());
            this.mapModule.setUpPirates();
            this.mapModule.setUpShips();
            this.setAllUsersRoomControllerEvents();
            this.emitToAllInRoom('gameHasBeenStarted');
        }
    };



    this.addRoomEventsToUserSocket = function (user) {
        var socket = user.getSocket();
        socket.on('ready', function () {
            user.setReady();
            that.emitter.emit('room:userIsReady', user.id);
        });

        socket.on('startGame', function () {
            that.startGame(user);
            var userName = that.getUserById(that.turn).name;
           // that.mapModule.emitter.emit('sendMessageToAll', {string :'Ход игрока ' + userName, hideStatus: false});
        })
    };

    this.leave = function (user) {
        this.removeUserFromRoom(user);
    };

    this.getUsersList = function () {
        var users = this.users,
            usersList = [];
        for (var key in users) {
            usersList.push(users[key].getPublicData());
        }
        return usersList;
    };

    this.getUsersListWithoutThisUser = function (user) {
        var users = this.users,
            usersList = [];
        for (var key in users) {
            if (key != user.id) {
                usersList.push(users[key].getPublicData());
            }
        }
        return usersList;
    };

    this.checkReadyList = function () {
        var users = this.users,
            bool = true;
        for (var key in users) {
            if (!users[key].ready) {
                bool = false;
                break;
            }
        }
        return bool;
    };

    this.emitToAllInRoom = function (handler, data) {
        var users = this.users;
        for (var key in users) {
            if (users.hasOwnProperty(key)) {
                users[key].getSocket().emit(handler, data)
            }
        }
    };

    this.getUserById = function (userId) {
        return (this.users[userId])? this.users[userId] : false;
    };

    this.initialize = function () {
        this.emitter.on('room:newUserJoined', function (user) {
            that.emitToAllInRoom('newUsersJoin', user.getPublicData());
        });

        this.emitter.on('room:userLeft', function (user) {
            that.emitToAllInRoom('userLeft', user.getPublicData());
        });

        this.emitter.on('room:leaderHasBeenChanged', function (newLeaderId) {
            that.emitToAllInRoom('newLeader', newLeaderId);
        });

        this.emitter.on('room:userIsReady', function (userId) {
            that.emitToAllInRoom('userIsReady', userId);
            if (that.checkReadyList()) {
                that.users[that.leader].getSocket().emit('readyToStart');
            }
        });

        this.mapModule.emitter.on('changeTurn', function () {
            that.changeTurn();
            var userName = that.getUserById(that.turn).name;

            that.mapModule.emitter.emit('sendMessageToAll', {string :'Ход игрока ' + userName, hideStatus: false});
        })

    };



    this.initialize();
};

exports.room = Room;