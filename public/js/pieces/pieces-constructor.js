var mapSize = 13;


var fillMap = function () {
    var map = [];
    for (var y = 0; y < mapSize; y++ )
    {
        for (var x = 0; x < mapSize; x++)
        {
            if (x == 0 || y == 0 || x == mapSize -1 || y == mapSize -1 || (x == 1 && y == 1) || (x ==  mapSize - 2 && y == 1) || (y == mapSize - 2 && x == mapSize - 2) || (x == 1 && y == mapSize - 2))
            {
                map.push({pieceId: 0, position: {x: x, y: y}});
            }
            else map.push({pieceId: 45,  position: {x: x, y: y}});
        }
    }
    return map;
}