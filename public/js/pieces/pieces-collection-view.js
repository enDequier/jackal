
var PiecesCollectionView = Backbone.View.extend({

    initialize: function () {
        this.render()
    },

    render: function () {
        this.collection.each(function (piecesModel) {
            var piecesModelView = new PiecesModelView({model: piecesModel})
        })
    }
});