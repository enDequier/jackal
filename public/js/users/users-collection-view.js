
var UsersCollectionView = Backbone.View.extend({

    initialize: function () {
        this.$el = $('.users');
        this.render();
        this.collection.on('add', function (model) {
            this.renderOne(model);
        },this);

        this.collection.on('remove', function () {
           // this.render();
            console.log('removeeeeeeeeee')
        }, this)
    },

    render: function () {
        this.collection.each(function (userModel) {
            var userModelView = new UserModelView({model: userModel})
        });
    },

    renderOne: function (model) {
        var userModelView = new UserModelView({model: model});
    }
});