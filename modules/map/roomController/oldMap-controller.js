
var MapModule = require('../../map/map').map;
var m_map = new MapModule();
var map = m_map.getMap();
var openedPieces = [];

function sendToAll (handler, data, sockets) {
    for (var i = 0; i < sockets.length; i++) {
        sockets[i].emit(handler, data);
    }
}


exports.controller = function (io_server) {
    io_server.on('connection', function(socket) {
        console.log('connected');

        if (m_map.openedPieces.length > 0) {
            socket.emit('MapPiece', m_map.openedPieces);
        }

        socket.emit('addPirates', m_map.getPirates());
        socket.emit('MapPiece', m_map.getShips('public'));


        socket.on('selectPirate', function (data) {
            m_map.selectedPirate = m_map.getPirateById(data.id);
            socket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
            socket.emit('variations', m_map.getPiratesVariations(m_map.selectedPirate));

        });

        socket.on('clickToolsButton', function (data) {
            if (data.status === 'pickup') {
                m_map.pickupResource(data.type, m_map.selectedPirate);
            }
            else if (data.status === 'drop') {
                m_map.dropResource(data.type, m_map.selectedPirate);
            }

            socket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
        });


        m_map.emitter.on('changePiratesPosition', function (pirates) {
            socket.emit('sendPirates', pirates); //all
            socket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
        });



        socket.on('changePiratesPosition', function (position) {
            m_map.movePirate(position);
        });


        m_map.emitter.on('sendMapPieces', function (pieces) {
            socket.emit('MapPiece', pieces);    //alll
        });

        m_map.emitter.on('sendPirates', function (pirates) {
            socket.emit('sendPirates', pirates);
        })





    });



};
