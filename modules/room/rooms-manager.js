

exports.roomsRouter = function (data) {

    var app = data.app,
        fs = data.fs,
        ejs = data.ejs,
        users = data.users,
        io = data.io,
        rooms = data.rooms;


    app.get('/room', function (req, res) {
        if (req.session.authorized) {
            users.addUser(req.session);
            var user = users.getUserById(req.session.user.id);
            if (user.roomId) {
                var oldRoom = rooms.getRoomById(user.roomId);
                if (oldRoom) {
                    if (oldRoom.getUserById(user.id)) {
                        res.send('Вы уже находитесь в комнате')
                    }
                }
            } else {
                var roomId = req.query.id;
                var room = rooms.getRoomById(roomId);
                if (room) {
                    if (room.usersCount >= 4) {
                        res.send('Комната переполнена');
                    } else if (room.open) {
                        if (!room.users.hasOwnProperty(user.id)) {

                            user.roomId = roomId;
                            res.render('game',{username: req.session.user.name});
                        } else {
                            res.send('Вы уже в этой комнате');
                        }
                    } else {
                        res.send('Игра уже запущена');
                    }

                } else {
                    res.render('index', {userName: req.session.user.name});
                }

            }

        } else {
            res.render('login', {});
        }
    });

    app.get('/create', function (req, res) {
        if (req.session.authorized) {
            var user = users.getUserById(req.session.user.id);
            if (user.roomId) {
                var oldRoom = rooms.getRoomById(user.roomId);
                if (oldRoom) {
                    if (oldRoom.getUserById(user.id)) {
                        res.send('Вы уже в другой комнате')
                    }
                }
            } else {
                var room = rooms.addRoom();
                res.redirect(room.href);
            }

        }
    });

    app.post('/ajax/roomslist', function (req, res) {
        if (req.session.authorized) {
            res.send({
                status: true,
                body: rooms.getOpenedRooms()
            });
        }
    });

    app.get('/rooms', function (req, res) {
        if (req.session.authorized) {
            var content = ejs.render(fs.readFileSync('views/rooms.ejs', 'utf-8'), {}),
                content = ejs.render(fs.readFileSync('views/index.ejs', 'utf-8'), {
                    userName: req.session.user.name,
                    content: content

                });
            res.send(content);
        }
    });


};

