var fs = require('fs'),
    ejs = require('ejs');



function checkMail (emailString) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(emailString);
}

exports.router = function (data) {
    var app = data.app;
    var DB = data.db;
    var rooms = data.rooms;



    app.get('/index', function(req, res) {
        if (!req.session.authorized) {
            res.render('login',{});
        }
        else {
            res.redirect('/home');
        }

        // console.log(req.session)
    });

    app.get('/home', function (req, res) {
        if (req.session.authorized) {
            var content = ejs.render(fs.readFileSync('views/home.ejs', 'utf-8'), {}),
                content = ejs.render(fs.readFileSync('views/index.ejs', 'utf-8'), {
                    userName: req.session.user.name,
                    content: content

            });
            res.send(content);
        } else {
            res.render('login',{});
        }
    });

    app.get('/', function (req, res) {
        res.redirect('/home');
    });


    app.get('/logout', function (req, res) {
        delete req.session.authorized;
        delete req.session.user;
        res.render('login',{});
    });



    app.get("*.css", function(req, res) {
        var path = __dirname + req.url;
        fs.readFile(path, "utf8", function(err, css) {
            res.header("Content-type", "text/css");
            res.send(css);
        });
    });

    app.get("*.js", function(req, res) {
        var path = __dirname + req.url;
        fs.readFile(path, "utf8", function(err, js) {
            res.header("Content-type", "text/javascript");
            res.send(js);
        });
    });

    app.get('/login', function(req, res) {
        if (!req.session.authorized) {
            res.render('login', {});
        } else {
            res.render('index',{});
        }
    });




    app.post('/login', function(req, res){
        if (!req.session.authorized) {
            var data = req.body;
            DB.query('select * from Users where login ="' + data.login + '"', function(error, result) {
                if (result.length > 0) {
                    var user = result[0];
                    if (user.password === data.password) {
                        req.session.authorized = true;
                        req.session.user = {
                            name: user.nickname,
                            id: user.id
                        };
                       // console.log(req.session, result);
                        res.send({status: true});
                    } else {
                        res.send({status: false});
                    }
                }
                else {
                    res.send({status: false});
                }
            });
        }
        else {
            res.render('index',{username: req.session.user.name});
        }

    });

    app.post('/registration', function(req, res){
        if (!req.session.authorized) {
            var data = req.body,
                bool = true;
            if (bool) {
                if (data.login.length < 5) {
                    res.send({status: false, message: 'длина Эл.Почты не меньше 5 символов'})
                    bool = false;
                }
            }

            if (bool) {
                if (!checkMail(data.login)) {
                    res.send({status: false, message: 'Некорректный адресс почты'});
                    bool = false;
                }
            }

            if (bool) {
                DB.query('select * from Users where login = "'+ data.login +'"', function(error, result) {
                    if (!error) {
                        if (result.length > 0) {
                            res.send({status: false, message: 'Эл.Почта уже зарегистрированна'});
                            bool = false;
                        }
                    }
                });
            }

            if (bool) {
                DB.query('select * from Users where nickname = "'+ data.nickname +'"', function(error, result) {
                    if (!error) {
                        if (result.length > 0) {
                            res.send({status: false, message: 'Псевдоним уже занят'});
                            bool = false;
                        }
                    }
                });
            }

            if (bool) {
                if (data.nickname.length < 2 || data.nickname.length > 12) {
                    res.send({status: false, message: 'Длина псевдонима 2-12 символов'});
                    bool = false;
                }
            }

            if (bool) {
                if (data.password.length > 8) {
                    if (data.password != data.password2) {
                        res.send({status: false, message: 'Пароли не совпадают'});
                    }
                } else {
                    res.send({status: false, message: 'Длина пароля не меньше 8 символов'});
                    bool = false;
                }
            }







            if (bool) {
                DB.query('insert into Users (login, password, nickname, registrationDate) values("'+ data.login +'","' + data.password +'","' + data.nickname +'","' + new Date().getTime() +'")', function(error, result) {
                    if (result.hasOwnProperty('insertId')) {
                        req.session.authorized = true;
                        req.session.user = {
                            name: data.nickname,
                            id: result.insertId
                        };
                        res.send({status: true, message: 'all is okey'});
                    } else {
                        res.send({status: false, message: 'Неизвестная ошибка'});
                    }
                });
            }
        }

    });

};

