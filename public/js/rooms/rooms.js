var roomsController = {
    $createButton: $('<a class="room" href="/create"><img class="room-picture" src="../public/img/main/island-bnw.jpg" alt="комната"/><span class="room-id">Создать комнату</span></a>'),
    $container: $('<div class="rooms"></div>'),
    getTmp: function (id, href, usersCount) {
        var $users = $('<div class="bay"></div>'),
            $tmp = $('<a class="room" href="'+ href +'"><img class="room-picture" src="../public/img/main/island.jpg" alt="комната"/><span class="room-id">Номер комнаты: <span class="id">'+ id +'</span></span></a>'),
            i;

        for (i = 0; i < usersCount; i += 1) {
            $users.append('<div class="pirates-ships-in"></div>');
        }

        $tmp.append($users);

        return $tmp
    },

    getRoomsList: function () {
        var that = this;
        $.ajax({
            url: '/ajax/roomslist',
            method: "post",
            dataType: 'JSON'
        }).done(function(data) {
            if (data.status) {
               that.appendRooms(data.body);
            }
        });
    },

    appendRooms: function (rooms) {
        var roomsCount = rooms.length,
            i,
            room,
            $room;

        this.$container.empty();
        this.$content.append(this.$container);
        this.$container.append(this.$createButton);
        for (i = 0; i < roomsCount; i += 1) {
            room = rooms[i];
            $room = this.getTmp(room.id, room.href, room.usersCount);
            this.$container.append($room);
           // console.log($room)
        }
    },

    init: function () {
        var that = this;
        this.$content = $('.content');
    }
};

