
var EventEmitter = require('events').EventEmitter;
var User = function (userData, io) {
    var that = this;
    this.io = io;
    this.emitter = new EventEmitter();
    this.id = userData.id;
    this.name = userData.name;
    this.socketId = false;
    this.ready = false;
    this.roomId = false;
    this.leader = false;
    this.getSocket = function () {
        return io.sockets.socket(this.socketId);
    };

    this.setReady = function () {
        this.emitter.emit('user-ready');
        this.ready = true;
    };

    this.getPublicData = function () {
        return {
            id: this.id,
            name: this.name,
            ready: this.ready,
            leader: this.leader
        };
    };

    this.setDefaults = function () {
        this.socketId = false;
        this.ready = false;
        this.roomId = false;
        this.leader = false;
    };

    this.initialize = function () {
        this.emitter.on('users:setSocketToUser', function () {
            var client = that.getSocket();
        });
    };



    this.initialize();
};

exports.user = User;