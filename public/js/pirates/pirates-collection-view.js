var PiratesCollectionView = Backbone.View.extend({
    initialize: function () {
        this.render();
        this.collection.on('add', function (model) {
            this.renderOne(model);
        },this);
    },

    render: function () {
        this.collection.each(function (piratesModel) {
            var piratesModelView = new PiratesModelView({model: piratesModel})
        })
    },

    renderOne: function (model) {
        var piratesModelView = new PiratesModelView({model: model});
    }
});