var PiratesModelView = Backbone.View.extend({


    initialize: function () {
        this.$el = $('<div class="pirate"><div class="text"></div><div class="pirates-resource pirates-resource-gold"></div><div class="pirates-resource pirates-resource-coin">1</div></div>');
        var colorClass = 'pirates' + this.model.get('color');
        this.$el.$text = this.$el.find('.text');
        this.$el.addClass(colorClass);
        this.render();
        //console.log('Pirate ready to fight');

        this.model.on('change:position', function() {
            this.render();
        }, this);

        this.model.on('change:resources', function () {
            this.applyResourceStatus();
        }, this);

        this.model.on('remove', function () {
            this.remove();
        }, this);

        this.model.on('change:name', function() {
            this.$el.$text.text(this.model.get('name'));
        }, this);

        this.model.on('change:penalties', function() {
            console.log('change');
            var penalties = this.model.get('penalties');
            this.$el.$text.text((penalties > 0)? penalties : this.model.get('name'));
        }, this);
    },

    events: {
        'click': 'makeActive'
    },

    render: function () {
        events.trigger('pirates-model-view:newPosition', {$element: this.$el, position: this.model.get('position')});
        this.applyResourceStatus();
        this.$el.$text.text(this.model.get('name'));
        //console.log('pirate rendered')
    },

    makeActive: function () {
        $('.pirate').removeClass("active");
        this.$el.addClass('active');
        events.trigger('pirates-model-view:pirateSelected', {id : this.model.get('id'), 'timestamp': new Date().getTime()});
    },


    showResource: function (selector) {
        this.$el.find('.pirates-resource').hide();
        this.$el.find('.'+selector).show();
    },

    hideResource: function () {
        this.$el.find('.pirates-resource').hide();
    },

    applyResourceStatus: function () {
        var resources = this.model.get('resources');
        if (resources.coin.length === 1) {
            this.showResource('pirates-resource-coin');
        }

        else if (resources.gold.length === 1) {
            this.showResource('pirates-resource-gold');
        }

        else {
            this.hideResource();
        }
    }




});








