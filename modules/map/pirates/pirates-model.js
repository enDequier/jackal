
var Pirates = function (id, userId, name, position, color) {
        this.is = 'pirate';
        this.color = color;
        this.userId = userId;
        this.id = id;
        this.name = name;
        this.resources = {
            coin: [],
            gold: [],
            rum: []
        };
        this.position = position;
        this.previousPosition = null;
        this.penalties = 0;
        this.lastPieceOptions = {
            type: false,
            subType: false
        }
    };

exports.pirates = Pirates;