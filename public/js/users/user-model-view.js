
var UserModelView = Backbone.View.extend({
    tmp: _.template('<div class="user"><div class="user-picture"></div><div class="user-name"><%= name %></div></div>'),


    initialize: function () {
        var that = this;
        this.$el = $(this.tmp(this.model.toJSON()));
        this.$el.readyButton = $('<div class="ready-button">Готов</div>');
       // this.$el.unReadyButton = $('<div class="unready-button">Не готов</div>')
        this.render();
        this.model.on('remove', function () {
            this.remove();
        }, this);
        console.log(this.model);

        this.model.on('change:leader', function () {
            if (this.checkLeader()) {
                this.makeALeader();
            }
        },this);

        this.model.on('change:turn', function () {
            console.log('newTurn');
            var turn = this.model.get('turn');
            if (turn) {

                $('.user-name').removeClass('active');
                this.$el.find('.user-name').addClass('active');
            }
        },this);

        if (this.checkLeader()) {
            this.makeALeader();
        }

        if (this.model.get('you')) {
            this.setYou();
        }

        this.model.on('change:ready', function () {
            this.setReady()
        },this);

        if (this.model.get('ready')) {
            this.setReady();
        }


        this.$el.readyButton.on('click', function () {
            events.trigger('user-model-view:ready');
        });
    },

    render: function () {
        $('.users-container').append(this.$el);
    },

    makeALeader: function () {
        $('.user').removeClass('leader');
        this.$el.addClass('leader');
        if (this.model.get('you') && this.model.get('leader')) {
            //$('.start-game').fadeIn(1000);
        }
    },

    setYou: function () {
        var id = this.model.get('id');
        $('.black-line').append(this.$el.readyButton);
    },

    checkLeader: function () {
        return this.model.get('leader')
    },

    setReady: function () {
        this.$el.addClass('ready');
        this.$el.readyButton.remove();

    }



});