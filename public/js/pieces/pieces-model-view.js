
var PiecesModelView = Backbone.View.extend({
    tmp: _.template('<div class="small piece-form"><div class="tmp small piece piece-<%= pieceId %>"><div class="coin"></div><div class="gold"></div><div class="rum"></div></div><div class="small pirates"></div></div>'),


    initialize: function () {
        this.model.set('view', this); // добавляем ссылку на вью в модель
        this.$el = $(this.tmp(this.model.toJSON()));

        this.model.on('change:pieceId', function() {
            var pieceId = this.model.get('pieceId');
            //console.log('changeTo >>>>>>', pieceId);
            var $el = this.$el.find('.piece');
            $el.removeAttr('class').addClass('small piece piece-' + pieceId);
        }, this);

        this.model.on('change:rotatable', function() {
            this.$el.find('.piece').rotate(this.model.get('rotatable') * 90);
           // console.log(this.model.get('rotatable'));
        }, this);

        this.model.on('change:resources', function () {
            var resources = this.model.get('resources');
            if (resources.coin.length > 0) {
                this.$el.find('.coin').text(resources.coin.length).show();
            }
            else {
                this.$el.find('.coin').hide();
            }

            if (resources.rum.length > 0) {
                this.$el.find('.rum').text(resources.rum.length).show();
            }
            else {
                this.$el.find('.rum').hide();
            }

            this.$el.find('.gold')[(resources.gold.length > 0)? 'show' : 'hide']();

        }, this);

        this.render();

        this.model.on('change:visited', function () {
            var $element = this.model.get('pirate$element');

            this.addPirate($element);
        }, this);
    },

    events: {
        'click' : 'makeStep'
    },

    render: function () {
        $('.map').append(this.$el)
    },

    makeIlluminated : function () {
        this.$el.addClass('illuminated');
    },

    makeStep: function () {
        if (this.$el.hasClass('illuminated')) {
            events.trigger('pieces-model-view:PirateMakeMove', this.model.get('position'));
        }
    },

    addPirate: function ($element) {
        //console.log('yeah');
        this.$el.find('.pirates').append($element);
    }

    /*
    getMapPiece: function () {
        events.trigger('pieces-model-view:getMapPiece', this.model);
    }   */

    /*appendPirate: function (PM,PV) {
        if (this.model.get('position').x == PM.get('position').x && this.model.get('position').y == PM.get('position').y) {
            PV.viewIn(this.$el)
        }
    }    */

})