
var events = {};
    _.extend(events, Backbone.Events);
var socket = io.connect('http://node.dev.market-helper.ru');
var map = fillMap();
var piecesCollection = new PiecesCollection(map);
var piratesCollection = new PiratesCollection();
var usersCollection = new UsersCollection();

function showMessage (string, hide) {
    var $textContainer = $('.message .text');
        $textContainer.hide();
        $textContainer.text(string);
        $textContainer.fadeIn(500, function () {
            if (hide) {
                $textContainer.fadeOut(3000);
            }
        });
}


function  setModelProperties (model, properties) {
    for (var key in properties)
    {
        model.set(key, properties[key]);
    }
}

function getModelByPosition (collection, position) {
    var mod = collection.find(function(model) {
        var pos = model.get('position');
        if (pos.x == position.x &&  pos.y == position.y) {
            return model;
        }
    });
    return mod;
}

function setPieceProperties (prop, piecePosition, collection) {
    var piece = getModelByPosition(collection, piecePosition);
    setModelProperties(piece, prop);
}

function addUsersInRoom (users) {
    usersCollection.add(users);
}


$('document').ready(function () {
    window.toolsModel = new ToolsModel();
    var toolsView = new ToolsModelView({model: toolsModel}),
        piecesCollectionView = new PiecesCollectionView({collection: piecesCollection}),
        usersCollectionView = new UsersCollectionView({collection: usersCollection}),
        piratesCollectionView = new PiratesCollectionView({collection: piratesCollection});

    socket.on('turn', function (userId) {
        usersCollection.each(function(user) {
            user.set('turn', false)
        });
        usersCollection.findWhere({id: userId}).set('turn', true);
        $('.piece-form').removeClass('illuminated');
        $('.pirate').removeClass('active');
        $('.darkness').hide();
    });

    socket.on('readyToStart', function () {
        $('.start-game').addClass('active');
    });

    socket.on('notReady', function () {
        $('.start-game').removeClass('active');
    });

    socket.emit('joined');

    socket.on('newLeader', function (userId) {
        usersCollection.findWhere({id: userId}).set('leader', true);
    });

    socket.on('newUsersJoin', function (users) {
        addUsersInRoom(users)
    });

    socket.on('usersInRoom', function (users) {
        addUsersInRoom(users)
    });

    events.on('user-model-view:ready', function () {
        socket.emit('ready');
    });

    socket.on('finalResults', function (results) {
        var $table = $('.results-table');
        for (var key in results) {
            if (results.hasOwnProperty(key)) {
                var $node = $('<tr><td>' + key + '</td><td><span class="coin-count">' + results[key] +'</span></td></tr>');
                $table.append($node);
            }
        }
        $('.results').show();
    });

    socket.on('userIsReady', function (userId) {
        usersCollection.findWhere({id: userId}).set('ready', true);
    });

    socket.on('pirateHasBeenExiled', function (pirate) {
        piratesCollection.remove(pirate);
        $('.piece-form').removeClass('illuminated');
        $('.pirate').removeClass('active');
        $('.darkness').hide();
    });

    socket.on('userLeft', function (user) {
        var user = usersCollection.findWhere({id: user.id});
        if (typeof user != "undefined") {
            usersCollection.remove([user]);
        }
    });

    events.on('pirates-model-view:pirateSelected', function(data) {
        socket.emit('selectPirate', data);
        //console.log(data)
    });

    events.on('pieces-model-view:PirateMakeMove', function(position) {
        socket.emit('changePiratesPosition', position);

    });

    socket.on('resourceStatus', function (data) {
        toolsModel.set('coinStatus', data.coin);
        toolsModel.set('goldStatus', data.gold);
        toolsModel.set('rumStatus', data.rum);
    });

    socket.on('sendPirates', function (pirates) {
        if ($.isArray(pirates)) {
            for (var i = 0; i < pirates.length; i++) {
                setModelProperties(piratesCollection.findWhere({id : pirates[i].id}), pirates[i]);
            }
        }
        else {
            setModelProperties(piratesCollection.findWhere({id : pirates.id}), pirates);
        }

        $('.piece-form').removeClass('illuminated');
        $('.pirate').removeClass('active');
        $('.darkness').hide();
    });

    socket.on('variations', function (variations) {
        $('.piece-form').removeClass('illuminated');
        var piece;
        for (var key in variations) {
            piece =  getModelByPosition(piecesCollection, variations[key]);
            piece.get('view').makeIlluminated();
        }
        $('.darkness').show();
    });

    socket.emit('addPirates');
    socket.on('addPirates', function (pirates) {
        console.log('getPirates ===== >>>', pirates);
        for (var i = 0; i < pirates.length; i++) {
            piratesCollection.add(pirates[i]);
        }

    });

    events.on('pirates-model-view:newPosition', function (pirate) {
        var piece =  getModelByPosition(piecesCollection, pirate.position);
        piece.set('pirate$element', pirate.$element);
        piece.set('visited', piece.get('visited') + 1);
    });

    events.on('tools-model-view:clickToolsButton', function (data) {
        socket.emit('clickToolsButton', data);
    });

    socket.on('MapPiece', function (data) {
        if ($.isArray(data)) {
            for (var i = 0; i < data.length; i++) {
                setPieceProperties(data[i], data[i].position, piecesCollection)
            }
        }
        else {
            setPieceProperties(data, data.position, piecesCollection)
        }

    });

    $('.darkness').on('click', function() {
        $('.piece-form').removeClass('illuminated');
        $(this).hide();
    });

    socket.on('message', function (data) {
       // console.log(data)
        showMessage(data.string, data.hideStatus);
    });

    socket.on('gameOver', function () {
        $('.tools').hide();
        $('.map').slideUp().remove();

    });

    $('.start-game').click(function () {
        if ($(this).hasClass('active')) {
            socket.emit('startGame');
        }
    });

    socket.on('gameHasBeenStarted', function () {
        $('.ship').fadeOut(800, function () {
            $('.users').addClass('minimize');
            $('.tools').show();
            $('.start-game').remove();
            $('.map').slideDown();
        });

    });
});