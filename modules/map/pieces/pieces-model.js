

var Piece = function (pieceId, position, type, subType, rotatable, name, walkingWithCoin) {
    this.public = {
            position : position,    // {x: int, y: int}
            pieceId : pieceId,      // int
            rotatable: rotatable,
            variations: undefined,
            resources: {
                rum: [],
                coin: [],
                gold: []
            }
    };
    this.is = 'piece';
    this.type = type || undefined; // string
    this.subType = subType || undefined; //
    this.opened = false;           //
    this.name = name || undefined;          // string
    this.walkingWithCoin = walkingWithCoin;

};


exports.piece = Piece;






