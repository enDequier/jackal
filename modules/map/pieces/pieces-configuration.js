var piecesConfiguration = {
    0: {
        name: 'Water',
        count: 52,
        type: 'water',
        subType: 'border',
        walkingWithCoin: false
    },

    1: {
        name: 'Gun',
        count: 2,
        type: 'gun',
        subType: 'gun',
        rotatable: 3,
        walkingWithCoin: true
    },

    2: {
        name: 'x2',
        count: 6,
        type: 'x2',
        subType: 'x2',
        walkingWithCoin: true
    },

    3: {
        name: 'Trap',
        count: 3,
        type: 'special',
        subType: 'trap',
        walkingWithCoin: true
    },

    4: {
        name: 'Clear field type 1',
        count: 4,
        type: 'land',
        subType: 'field',
        walkingWithCoin: true
    },

    5: {
        name: 'Chest x1',
        count: 5,
        resourceCount: 1,
        type: 'land',
        subType: 'resource',
        resourceType: 'coin',
        walkingWithCoin: true
    },

    6: {
        name: 'Gold ingot',
        count: 1,
        resourceCount: 1,
        type: 'land',
        subType: 'resource',
        resourceType: 'gold',
        walkingWithCoin: true
    },

    7: {
        name: 'Fortress',
        count: 2,
        type: 'land',
        subType: 'fortress',
        walkingWithCoin: false
    },

    8: {
        name: 'Clear field type 2',
        count: 5,
        type: 'land',
        subType: 'field',
        walkingWithCoin: true
    },

    9: {
        name: 'Horse',
        count: 2,
        type: 'horse',
        subType: 'horse',
        walkingWithCoin: true
    },

    10: {
        name: 'Missionary',
        count: 1,
        type: 'land',
        subType: 'missionary',
        walkingWithCoin: true
    },

    11: {
        name: 'Labyrinth x2',
        count: 5,
        type: 'special',
        subType: 'labyrinth',
        penalties: 2,
        walkingWithCoin: true
    },

    12: {
        name: 'Chest x4',
        count: 2,
        resourceCount: 4,
        type: 'land',
        subType: 'resource',
        resourceType: 'coin',
        walkingWithCoin: true
    },

    13: {
        name: 'Chest x2',
        count: 5,
        resourceCount: 2,
        type: 'land',
        subType: 'resource',
        resourceType: 'coin',
        walkingWithCoin: true
    },

    14: {
        name: 'Chest x3',
        count: 3,
        resourceCount: 3,
        type: 'land',
        subType: 'resource',
        resourceType: 'coin',
        walkingWithCoin: true
    },

    15: {
        name: 'Clear field type 3',
        count: 5,
        type: 'land',
        subType: 'field',
        walkingWithCoin: true
    },

    16: {
        name: 'Tunnel',
        count: 4,
        type: 'tunnel',
        subType: 'tunnel',
        walkingWithCoin: true
    },

    17: {
        name: 'Arrow x3',
        count: 0, // 3
        type: 'arrow',
        subType: 'three',
        rotatable: 3,
        walkingWithCoin: true
    },

    18: {
        name: 'Clear field type 4',
        count: 4,
        type: 'land',
        subType: 'field',
        walkingWithCoin: true
    },

    19: {
        name: 'Rum x1',
        count: 3,
        type: 'land',
        resourceCount: 1,
        subType: 'resource',
        resourceType: 'rum',
        walkingWithCoin: true
    },

    20: {
        name: 'Labyrinth x3',
        count: 4,
        type: 'special',
        subType: 'labyrinth',
        penalties: 3,
        walkingWithCoin: true
    },

    21: {
        name: 'Arrow x2 diagonal',
        count: 4, //3
        type: 'diagonal-arrow',
        subType: 'two',
        rotatable: 1 ,
        walkingWithCoin: true
    },

    22: {
        name: 'Cannabis',
        count: 2,
        type: 'land',
        subType: 'cannabis',
        walkingWithCoin: true
    },

    23: {
        name: 'Air balloon',
        count: 2,
        type: 'special',
        subType: 'airballoon',
        walkingWithCoin: true
    },

    24: {
        name: 'Arrow x4 diagonal',
        count: 4, //3
        type: 'diagonal-arrow',
        subType: 'four',
        walkingWithCoin: true
    },

    25: {
        name: 'Labyrinth x4',
        count: 2,
        type: 'special',
        penalties: 4,
        subType: 'labyrinth',
        walkingWithCoin: true
    },

    26: {
        name: 'Arrow x4',
        count: 4, // 3
        type: 'arrow',
        subType: 'four',
        walkingWithCoin: true
    },

    27: {
        name: 'Ogre',
        count: 1,
        type: 'special',
        subType: 'ogre',
        walkingWithCoin: true
    },

    28: {
        name: 'Arrow x1 diagonal',
        count: 3,
        type: 'diagonal-arrow',
        subType: 'one',
        rotatable: 3,
        walkingWithCoin: true
    },

    29: {
        name: 'Crocodile',
        count: 4,
        type: 'special',
        subType: 'crocodile',
        walkingWithCoin: true
    },

    30: {
        name: 'Rum x2',
        count: 2,
        type: 'land',
        resourceCount: 2,
        subType: 'resource',
        resourceType: 'rum',
        walkingWithCoin: true
    },

    31: {
        name: 'Rum x3',
        count: 1,
        type: 'land',
        resourceCount: 3,
        subType: 'resource',
        resourceType: 'rum',
        walkingWithCoin: true
    },

    32: {
        name: 'Friday',
        count: 1,
        type: 'land',
        subType: 'friday',
        walkingWithCoin: true
    },

    33: {
        name: 'Air plan',
        count: 1,
        type: 'special',
        subType: 'airplan',
        walkingWithCoin: true
    },

    34: {
        name: 'Rum barrels',
        count: 4,
        type: 'land',
        resourceCount: 0,
        subType: 'resource',
        resourceType: 'rum',
        walkingWithCoin: true
    },

    35: {
        name: 'Arrow x1 horizontal',
        count: 3,
        type: 'arrow',
        subType: 'one',
        rotatable: 3,
        walkingWithCoin: true
    },

    36: {
        name: 'Revival fortress',
        count: 1,
        type: 'revival',
        subType: 'fortress',
        walkingWithCoin: false
    },


    37: {
        name: 'Labyrinth x5',
        count: 1,
        type: 'special',
        subType: 'labyrinth',
        penalties: 5,
        walkingWithCoin: true
    },

    38: {
        name: 'Chest x5',
        count: 1,
        resourceCount: 5,
        type: 'land',
        subType: 'resource',
        resourceType: 'coin',
        walkingWithCoin: true
    },

    39: {
        name: 'Rift',
        count: 1,
        type: 'land',
        subType: 'rift',
        walkingWithCoin: true
    },

    40: {
        name: 'Peace field',
        count: 3,
        type: 'land',
        subType: 'peace',
        walkingWithCoin: false
    },

    41: {
        name: 'Ben Gun',
        count: 1,
        type: 'land',
        subType: 'bengun',
        walkingWithCoin: true
    },

    42: {
        name: 'Lighthouse',
        count: 1,
        type: 'land',
        subType: 'lighthouse',
        walkingWithCoin: true
    },

    43: {
        name: 'Arrow x2 horizontal',
        count: 3,
        type: 'arrow',
        subType: 'two',
        walkingWithCoin: true
    },

    44: {
        name: 'Caramba',
        count: 1,
        type: 'land',
        subType: 'caramba',
        walkingWithCoin: true
    }
};

exports.pieceConfiguration = piecesConfiguration;