var PiratesModel = Backbone.Model.extend({
    defaults: {
        id: undefined,
        position: {
            x: undefined,
            y: undefined
        },
        previousPosition : {
            x: undefined,
            y: undefined
        },
        penalties: 0,
        name: ''
    }
});