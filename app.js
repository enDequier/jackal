var express = require('express'),
    http = require('http'),
    url = require('url'),
    path = require('path'),
    Users = require('./modules/users/users-model.js').users,
    ejs = require('ejs'),
    fs = require('fs'),
    io = require('socket.io'),
    cookie = require("cookie"),
    connect = require("connect"),
    DB = require("./modules/database/db").db,
    MySQLStore = require('connect-mysql')(express),

    MySQLStoreOptions = {
        pool: true,
        config: {
            user: 'node_user',
            password: 'PuPiDuPu',
            database: 'node'
        }
    };




var app = express(),
    viewEngine = 'ejs';
var session_store = new MySQLStore(MySQLStoreOptions);
app.set('port',9091);
app.configure(function() {
    app.set('views', __dirname + '/views');
    app.set('view engine', viewEngine);

    app.use(express.cookieParser('heroOfDinoLand'));
    app.use(express.session({
        secret: 'heroOfDinoLand',
        store: session_store
    }));

    app.use(express.methodOverride());
    app.use(express.urlencoded());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});





var criptpassword = function(string){
    var crypto = require('crypto');
    return crypto.createHash('md5').update(string+global.saldo).digest("hex");
};

var server = http.createServer(app);
var io_server = io.listen(server, {log: false});
var roomsManager = require('./modules/room/rooms-manager');
var users = new Users(io_server);
var RoomsCollector = require ('./modules/room/rooms-collector').roomsCollector;
var rooms = new RoomsCollector(io_server);
console.log(rooms);
rooms.addRoom(0);
var roomsRouter = roomsManager.roomsRouter({
    app: app,
    fs: fs,
    ejs: ejs,
    users: users,
    rooms: rooms,
    io: io_server
});

var router = require('./routers/router').router({
    app: app,
    db: DB,
    rooms: rooms
});



var getSessionData = function (headersCookie, secret) {
    var cookies = cookie.parse(headersCookie);
    var sid = connect.utils.parseSignedCookies(cookies, secret)['connect.sid'];
    console.log(sid);
    if (sid) {
        session_store.get(sid, function (error, session) {
            console.log('get session time', new Date().getTime());
            console.log(session);
            return session;
        });
    }
    else {
        return false
    }
};


io_server.set('authorization', function (data, accept) {
    if (!data.headers.cookie) {
        return accept('No cookie transmitted.', false);
    }
    var cookies = cookie.parse(data.headers.cookie);
    var sid = connect.utils.parseSignedCookies(cookies, 'heroOfDinoLand')['connect.sid'];
    //console.log(sid);
    if (sid) {
        session_store.get(sid, function (error, session) {
            return accept(null, true);

        });
    }
    else {
        return accept('Authorization failed', false);
    }


});
console.log(users.users);

io_server.on('connection', function (socket) {
    var ready = false,
        room = false,
        user;
    var cookie_string = cookie.parse(socket.handshake.headers.cookie);
    var sid = connect.utils.parseSignedCookies(cookie_string, 'heroOfDinoLand')['connect.sid'];
    if (sid) {
        session_store.get(sid, function (error, session) {
            if (session) {
                user = users.setSocketToUser(session.user.id, socket);
                ready = true;
            }
        });
    }



    socket.on('joined', function () {
        if (user) {
            rooms.addUserInRoom(user);
        }
    });

    socket.on('disconnect', function () {
        rooms.removeUserFromRoom(user);
    });

});



server.listen(app.get('port'));