



exports.RoomController = function (m_map) {
    var map = m_map.getMap();


     this.setEventsToUser = function (user, room) {
         var userSocket = user.socketId;
             userSocket = user.io.sockets.socket(userSocket);
         if (m_map.openedPieces.length > 0) {
             userSocket.emit('MapPiece', m_map.openedPieces);
         }

         userSocket.emit('addPirates', m_map.getPirates());
         userSocket.emit('MapPiece', m_map.getShips('public'));
       /*  for (var i = 0; i < map.length; i++) {
             for (var j = 0; j < map[i].length; j++) {
                 userSocket.emit('MapPiece', map[i][j].public);
             }
         }     */


        userSocket.on('selectPirate', function (data) {
            if (room.id == user.roomId) {
                var pirate = m_map.getPirateById(data.id),
                    bool = false;

                if (room.turn == user.id && pirate) {
                    if (pirate.userId == user.id) {
                        if (m_map.canSelectPirate) {
                            m_map.selectedPirate = pirate;
                            bool = true;
                        } else {
                            bool = (pirate.id == m_map.selectedPirate.id);
                        }
                        if (bool) {
                            if (m_map.canSelectPirate) {
                                m_map.getPiratesVariations(m_map.selectedPirate);
                            }
                            console.log('PIRATES VARIATIONS', m_map.currentVariation);
                            userSocket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
                            userSocket.emit('variations', m_map.currentVariation);
                        }
                    }
                }
            }
        });

         m_map.emitter.on('nextStep', function () {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     //console.log("NEXT STEEEP");
                     m_map.getPiratesVariations(m_map.selectedPirate);
                     console.log('NEXT STEP', new Date().getTime());
                     userSocket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
                     userSocket.emit('variations',  m_map.currentVariation);
                 }
             }
         });

         userSocket.on('clickToolsButton', function (data) {
             if (room.id == user.roomId) {
                 if (m_map.selectedPirate.userId == user.id) {
                     if (data.status === 'pickup') {
                         m_map.pickupResource(data.type, m_map.selectedPirate);
                     }
                     else if (data.status === 'drop') {
                         m_map.dropResource(data.type, m_map.selectedPirate);
                     }
                     userSocket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
                 }
             }
         });


         m_map.emitter.on('changePiratesPosition', function (pirates) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     room.emitToAllInRoom('sendPirates', pirates);              //all
                     userSocket.emit('resourceStatus', m_map.getResourceStatusForTools(m_map.selectedPirate));
                 }
             }
         });



         userSocket.on('changePiratesPosition', function (position) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                    m_map.movePirate(position);
                 }
             }
         });

         m_map.emitter.on('pirateHasBeenExiled', function (pirate) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     room.emitToAllInRoom('pirateHasBeenExiled', pirate);
                 }
             }
         });


         m_map.emitter.on('sendMapPieces', function (pieces) {
             if (room.id == user.roomId) {
                 room.emitToAllInRoom('MapPiece', pieces);    //all
             }
         });


         m_map.emitter.on('sendMessage', function (data) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     userSocket.emit('message', data);
                 }
             }
         });

         m_map.emitter.on('sendMessageToAll', function (text) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     room.emitToAllInRoom('message', text);
                 }
             }
         });

         m_map.emitter.on('checkGameStatus', function () {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     if (m_map.endOfGame()) {
                         room.emitToAllInRoom('gameOver');
                     }
                 }
             }
         });

         m_map.emitter.on('addPirates', function (pirates) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     room.emitToAllInRoom('addPirates', pirates);
                 }
             }
         });

         m_map.emitter.on('sendFinalResults', function (results) {
             if (room.id == user.roomId) {
                 if (room.turn == user.id) {
                     room.emitToAllInRoom('finalResults', results);
                 }
             }
         });



     }



};
