var ToolsModel = Backbone.Model.extend({
    defaults: {
        pickupText: 'Поднять',
        dropText: 'Бросить',
        nothingText: 'Нет действий',
        useText: 'Использовать',

        coinButton: '.coin-button',
        goldButton: '.gold-button',
        rumButton: '.rum-button',

        coinStatus: false,
        goldStatus: false,
        rumStatus: false
    }
});