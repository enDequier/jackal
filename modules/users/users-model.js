var User = require('./user-model').user;
var Users = function (io_server) {
    this.io = io_server;
    this.users = {};
    this.length = 0;
    this.addUser = function (session) {
        if (!this.users.hasOwnProperty(session.user.id)) {
            var user = new User(session.user, this.io);
            this.users[session.user.id] = user;
            console.log(user);
            this.length += 1;
        }
    };

    this.getUserById = function (userId) {
        return (this.users.hasOwnProperty(userId))? this.users[userId] : false;
    };

    this.setSocketToUser = function (userId, socket) {
        var user = this.getUserById(userId);

        if (user) {
            if (user.socketId) {
                user.socketId = false;
            }
            user.socketId = socket.id;
            user.emitter.emit('users:setSocketToUser');
            console.log('new user > ', user.name);
            return user;
        }
        else {
            console.error('No such user with this id');
        }
    };

    this.removeUser = function (userId) {
        delete this.users[userId];
        this.length -= 1;
    };


};

exports.users = Users;