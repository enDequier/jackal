var ToolsModelView = Backbone.View.extend({

    initialize: function () {
        this.$el = $('.tools');
        var model = this.model;
        this.$coinButton = this.$el.find(model.get('coinButton'));
        this.$goldButton = this.$el.find(model.get('goldButton'));
        this.$rumButton = this.$el.find(model.get('rumButton'));
        model.on('change:coinStatus', function () {
            this.editButton(this.$coinButton, model.get('coinStatus'));
        }, this);

        model.on('change:goldStatus', function () {
            this.editButton(this.$goldButton, model.get('goldStatus'));
        }, this);

        model.on('change:rumStatus', function () {
            this.editRumButton(this.$rumButton, model.get('rumStatus'));
        }, this);


        this.$coinButton.click(function () {
            var status = model.get('coinStatus');
            if (status != 'nothing') {
                events.trigger('tools-model-view:clickToolsButton', {status: status, type: 'coin'});
            }
        });

        this.$goldButton.click(function () {
            var status = model.get('goldStatus');
            if (status != 'nothing') {
                events.trigger('tools-model-view:clickToolsButton', {status: status, type: 'gold'});
            }
        });

        this.$rumButton.click(function () {
            var status = model.get('rumStatus');
            if (status != 'nothing') {
                events.trigger('tools-model-view:clickToolsButton', {status: status, type: 'rum'});
            }
        });


    },

    editButton: function ($el, status) {
        if (status === 'pickup') {
            $el.text(this.model.get('pickupText'));
        }

        else if (status === 'drop') {
            $el.text(this.model.get('dropText'));
        }

        else if (status === 'nothing') {
            $el.text(this.model.get('nothingText'));
        }

    },

    editRumButton: function ($el, status) {
        if (status === 'pickup') {
            $el.text(this.model.get('pickupText'));
        } else if (status === 'drop') {
            $el.text(this.model.get('useText'));
        } else if (status === 'nothing') {
            $el.text(this.model.get('nothingText'));
        }
    }


});


