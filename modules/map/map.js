

var Piece = require('./pieces/pieces-model').piece;
var Pirates = require('./pirates/pirates-model').pirates;
var Resource = require('./resource/resource-model').resource;
var EventEmitter = require('events').EventEmitter;



function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Array.prototype.shuffle = function( b ) {
    var i = this.length, j, t;
    while( i ) {
        j = Math.floor( ( i-- ) * Math.random() );
        t = b && typeof this[i].shuffle!=='undefined' ? this[i].shuffle() : this[i];
        this[i] = this[j];
        this[j] = t;
    }
    return this;
};

var Map = function () {
    this.configuration = require('./pieces/pieces-configuration').pieceConfiguration;
    var that = this;
    this.selectedPirate = undefined;
    this.map = [];
    this.mapSize = 13;
    this.pirates = [];
    this.ships = [];
    this.openedPieces = [];
    this.tunnels = [];
    this.canSelectPirate = true;
    this.users = false;
    this.currentVariation = [];
    this.gameOver = false;
    this.exiledResources = {
        rum: [],
        coin: [],
        gold: []
    };
    this.exiledPirates = [];
    this.emitter = new EventEmitter();

    this.setUsers = function (usersList) {
        this.users = usersList
    };

    this.sendFinalResults = function () {
        var results = {},
            ships = this.getShips(),
            shipsCount = ships.length,
            shipResources,
            i, j,
            score = 0;

        for (i = 0; i < shipsCount; i += 1) {
            score = 0;
            shipResources = ships[i].public.resources;
            if (shipResources.gold.length > 0) {
                score += shipResources.gold[0].score;
            }

            if (shipResources.coin.length > 0) {
                for (j = 0; j < shipResources.coin.length; j += 1) {
                    score += shipResources.coin[j].score;
                }
            }

            results[this.getUserById(ships[i].userId).name] = score;

        }
        return results;
    };

    this.endOfGame = function () {

        // true if 21 in one ship
        var ships = this.getShips(),
            shipsCount = ships.length,
            exiledResources = this.exiledResources,
            shipResources,
            i, j,
            score = 0,
            scoreInCycle,
            max = 0,
            winnerId;

        if (exiledResources.gold.length > 0) {
            score += exiledResources.gold[0].score;
        }

        if (exiledResources.coin.length > 0) {
            for (i = 0; i < exiledResources.coin.length; i += 1) {
                score += exiledResources.coin[i].score;
            }
        }

        for (i = 0; i < shipsCount; i += 1) {
            scoreInCycle = 0;
            shipResources = ships[i].public.resources;
            if (shipResources.gold.length > 0) {
                scoreInCycle += shipResources.gold[0].score;
            }

            if (shipResources.coin.length > 0) {
                for (j = 0; j < shipResources.coin.length; j += 1) {
                    scoreInCycle += shipResources.coin[j].score;
                }
            }


            if (max < scoreInCycle) {
                max = scoreInCycle;
                winnerId = ships[i].userId;
            }

            score += scoreInCycle;
        }

        if (score >= 15) {  // demo is 15 else 38
            this.gameOver = true;
            this.emitter.emit('sendMessageToAll', {string : 'AND THE WINNER IS ' + this.getUserById(winnerId).name, hideStatus: false});
            this.emitter.emit('sendFinalResults', this.sendFinalResults());
            return true;
        }
    };

    this.getUserById = function (userId) {
        var i = 0,
            users = this.users;

        if (users) {
            for (i = 0; i < users.length; i += 1) {
                if (users[i].id == userId) {
                    return users[i]
                }
            }
        }

        return [];
    };

    this.getPiecesList = function () {
        var pieces = [];
        for (var key in this.configuration) {
            if (key != 0) {
                var i = 0;
                while (i < this.configuration[key].count) {
                    pieces.push(key);
                    i++;
                }
            }
        }
        return pieces;
    };

    this.preparePiece = function (id, position) {
        var config = this.configuration[id];
        var rotate = (config.hasOwnProperty('rotatable'))? getRandomInt(0, config.rotatable) : 0;
        var piece = new Piece(id, position, config.type, config.subType, rotate, config.name, config.walkingWithCoin);
        if (config.subType === 'resource') {
            var resource = [];
            for (var i = 0; i < config.resourceCount; i++) {
                resource.push(new Resource(config.resourceType));
            }
            piece.public.resources[config.resourceType] = resource;
        }

        if (piece.subType == 'tunnel') {
            this.tunnels.push(piece);
        }

        if (piece.subType == 'labyrinth') {
            piece.penalties = config.penalties;
        }


        return piece;
    };


    this.shuffleMap = function () {
        var map = [];
        var pieces = this.getPiecesList().shuffle();
        var line = [];
        for (var y = 0; y < this.mapSize; y++)
        {
            for (var x = 0; x <  this.mapSize; x++)
            {
                if (x == 0 || y == 0 || x ==  this.mapSize -1 || y ==  this.mapSize -1 || (x == 1 && y == 1) || (x == this.mapSize - 2 && y == 1) || (y ==  this.mapSize - 2 && x ==  this.mapSize - 2) || (x == 1 && y ==  this.mapSize - 2))
                {
                    var water = new Piece(0, {x: x, y: y}, this.configuration[0].type, this.configuration[0].subType, 0, this.configuration[0].name, this.configuration[0].walkingWithCoin);
                    water.opened = true;
                    line.push(water);
                }
                else {
                    var id = pieces.pop();

                    var piece = this.preparePiece(id, {x: x, y: y});
                    line.push(piece);
                }
            }
            map.push(line);
            line = [];
        }
        return map;
    };


    this.getMap = function () {
        if (!this.map.length > 0) {
           this.map = this.shuffleMap();
        }
        return this.map;
    };

    this.getPiecesByPosition = function (position, map) {
        var pieces = [];
        map = map || this.map;

        if (position.length > 1) {
            for (var i = 0; i < position.length; i++) {
                pieces.push(map[position[i].y][position[i].x]);
            }
        }

        else {
            pieces = map[position.y][position.x];
        }


        return pieces;
    };

    this.swapPieces = function (from, to) {    // from, to - position
        var piece1 = this.getPiecesByPosition(from);
        var piece2 = this.getPiecesByPosition(to);
        var data = [piece1.public, piece2.public];
        piece1.public.position = to;
        piece2.public.position = from;
        this.map[to.y][to.x] = piece1;
        this.map[from.y][from.x] = piece2;
        this.emitter.emit('sendMapPieces', data);
    };

    this.addPirate = function (id, userId, position, color) {
        var name = this.getUserById(userId).name[0];
        var pirate = new Pirates (id, userId, name, position, color);
        this.pirates.push(pirate);
    };

    this.setUpShips = function () {
        var usersList = this.users,
            usersCount = usersList.length,
            startPositions = (usersCount === 2)? [{x: 6, y: 0}, {x: 6, y: 12}] : [{x: 6, y: 0}, {x: 12, y: 6}, {x: 6, y: 12}, {x: 0, y: 6}];

        for (var i = 0; i < usersCount; i++) {
            var ship = this.getPiecesByPosition(startPositions[i], this.map);
            ship.public.pieceId = (usersCount === 2 && i === 1)? 'ship' + 2  :'ship' + i;
            ship.userId = usersList[i].id;
            ship.type = 'ship';
            ship.name = 'Ship';
            ship.opened = true;
            ship.subType = (i % 2)? 'y' : 'x';
            ship.walkingWithCoin = true;
            this.ships.push(ship);
        }

    };

    this.getShipByUserId = function (userId) {
            var ships = this.ships,
                shipsLength = ships.length,
                i,
                ship = false;
            for (i = 0; i < shipsLength; i += 1) {
                if (ships[i].userId == userId) {
                    ship = ships[i];
                }
            }
            return ship;
    };

    this.getShips = function (prop) {
        this.emitter.emit('ships');
        if (prop == 'public') {
            var publicPropOfShips = [];
            for (var i = 0; i < this.ships.length; i++) {
                publicPropOfShips.push(this.ships[i].public);
            }
            return publicPropOfShips;
        }
        else {
            return this.ships;
        }
    };

    this.getPirates = function () {
        return this.pirates;
    };

    this.isType = function (typeName, variations) {
        var pieces = this.getPiecesByPosition(variations);
        if (variations.length > 0) {
            for (var i = 0; i < variations.length; i++) {
                if (pieces[i].type != typeName) {
                    return false
                }
            }
        }
        else {
            if (pieces.type != typeName) {
                return false
            }
        }

        return true;
    };

    this.isShipping = function (from, to) {
        var fromType = this.getPiecesByPosition(from).type;
        var toType = this.getPiecesByPosition(to).type;
        return (fromType == 'ship' && toType == 'water');
    };

    this.moveShip = function (from, to) {
        var pirates = this.getPiratesByPosition(from);
        for (var i = 0; i < pirates.length; i++) {
            pirates[i].previousPosition = from;
            pirates[i].position = to;

        }
        this.swapPieces(from, to);
        return pirates;
    };

    this.setUpPirates = function () {
        var pirateId = 0,
            i, j, // counters
            usersList = this.users,
            usersCount = usersList.length,
            startPositions = (usersCount === 2)? [{x: 6, y: 0}, {x: 6, y: 12}] : [{x: 6, y: 0},  {x: 12, y: 6}, {x: 6, y: 12}, {x: 0, y: 6}];

        for (i = 0; i < usersCount; i += 1) {

            for (j = 0; j < 3; j += 1) {
                pirateId += 1;
                this.addPirate(pirateId, usersList[i].id, startPositions[i], i);
            }
        }
    };

    this.getPirateById = function (id) {
        var pirate = false;
        for (var i = 0; i < this.pirates.length; i++) {
            if (this.pirates[i].id == id) {
                pirate = this.pirates[i];
            }
        }
        return pirate;
    };

    this.getPiratesByPosition = function (position) {
        var pirates = [];
        for (var i = 0; i < this.pirates.length; i++) {
            if (this.positionEquality(this.pirates[i].position, position)) {
                pirates.push(this.pirates[i]);
            }
        }
        return pirates;
    };

    this.exilePirate = function (pirate) {
        var pirates = this.pirates,
            piratesLength = pirates.length,
            newPirates = [],
            i;

        if (this.haveResource(pirate).gold) {
            this.exileResource('gold');
        } else if (this.haveResource(pirate).coin) {
            this.exileResource('coin');
        }

        for (i = 0; i < piratesLength; i += 1) {
            if (pirates[i].id != pirate.id) {
                newPirates.push(pirates[i]);
            } else {
                this.exiledPirates.push(pirates[i]);
            }
        }
        this.pirates = newPirates;
        this.emitter.emit('pirateHasBeenExiled', pirate)
    };

    this.haveExiledPirates = function (pirate) {
        var i,
            result = false,
            exiledPirates = this.exiledPirates,
            exiledPiratesLength = this.exiledPirates.length;

        if (exiledPiratesLength > 0) {
            for (i = 0; i < exiledPiratesLength; i += 1) {
                if (exiledPirates[i].userId == pirate.userId) {
                    result = true;
                    break
                }
            }
        }

        return result;
    };

    this.resurrectPirate = function () {
        var i,
            pirate,
            exiledPirates = this.exiledPirates,
            exiledPiratesLength = this.exiledPirates.length;

        if (exiledPiratesLength > 0) {
            for (i = 0; i < exiledPiratesLength; i += 1) {
                if (exiledPirates[i].userId == this.selectedPirate.userId) {
                    pirate = exiledPirates.splice(i, 1)[0];
                    break;
                }
            }
        }

        if (pirate) {
            pirate.position = this.selectedPirate.position;
            pirate.lastPieceOptions.type = 'land';
            pirate.lastPieceOptions.subType = 'fortress';
            this.pirates.push(pirate);
            this.emitter.emit('addPirates', [pirate]);
        }


    };

    this.isTrap = function (piece) {
        var bool = false,
            kindOfTrap = piece.subType;

        if (kindOfTrap == 'labyrinth' ||
            kindOfTrap == 'trap' ||
            kindOfTrap == 'ogre' ||
            kindOfTrap == 'cycle') {

            bool = true;
        }
        return bool;
    };

    this.traps = {
        labyrinth: function (pirate) {
            if (pirate.penalties > 0) {
                pirate.penalties -= 1;
                that.emitter.emit('sendMessageToAll', {string : 'Игрок ' + that.getUserById(pirate.userId).name + ' пропускает ход в лабиринте', hideStatus: false});
            } else {
                var piece = that.getPiecesByPosition(pirate.position);
                pirate.penalties = piece.penalties;
            }
            that.emitter.emit('changePiratesPosition', pirate);
        },

        trap: function (pirate) {
            var pirates = that.getPiratesByPosition(pirate.position),
                i;

            if (pirates.length > 1) {
                for (i = 0; i < pirates.length; i += 1) {
                    pirates[i].penalties = 0;
                    if (pirates[i].id != pirate.id) {
                        that.emitter.emit('changePiratesPosition', pirates[i]);
                    }
                }
            } else {
                pirate.penalties = 1;
            }
            that.emitter.emit('changePiratesPosition', pirate);
        },

        ogre: function (pirate) {
            that.exilePirate(pirate);
            that.emitter.emit('sendMessage', {string: 'Пират погиб в схватке с людоедом', hideStatus: false});
        },

        cycle: function (pirate) {
            var piece = that.getPiecesByPosition(pirate.position);
                piece.subType = that.configuration[piece.public.pieceId].subType;
            that.exilePirate(pirate);
            that.emitter.emit('sendMessage', {string: 'Пират попал в цикл', hideStatus: false});
        }


    };

    this.pieceTypesActions = {
        'ship': function (pirate) {
            var piece = that.getPiecesByPosition(pirate.position);
            if (piece.userId != that.selectedPirate.userId) {
                that.pirateHasBeenKilledByOtherPirate(that.selectedPirate);
            }

            if (that.haveResource(that.selectedPirate).coin || that.haveResource(that.selectedPirate).gold) {
                var resources = that.selectedPirate.resources;
                for (var key in resources) {
                    if (resources[key] && key != 'rum') {
                        that.dropResource(key, pirate);
                    }
                }
                that.emitter.emit('checkGameStatus');
            }

            that.emitter.emit('changePiratesPosition', pirate);
            that.endOfTurn();

        },

        'revival': function (pirate) {
            if (pirate.lastPieceOptions.type == 'revival') {
                that.resurrectPirate();
            }
            that.emitter.emit('changePiratesPosition', pirate);
            that.endOfTurn();
        },

        'tunnel': function (pirate) {

            that.emitter.emit('changePiratesPosition', pirate);
            if (pirate.lastPieceOptions.type = 'land' && that.getActiveTunnelsVariations(pirate).length > 0) {
                that.nextStep();
            } else {
                that.endOfTurn();
            }

        },

        'water': function (pirate) {
            that.emitter.emit('changePiratesPosition', pirate);
            that.endOfTurn();
        },

        'land': function (pirate) {
            that.emitter.emit('changePiratesPosition', pirate);
            that.endOfTurn();
        }






    };



    this.movePirate = function (position) {

        var variations = this.currentVariation,
            piece = this.getPiecesByPosition(position),
            bool = false,
            pirate,
            i;
        for (i = 0; i < variations.length; i++) {
            if (this.positionEquality(variations[i], position)) {
                bool = true;
                break;
            }
        }

        if (bool) {

            pirate = this.selectedPirate;
            if (this.isShipping(pirate.position, position)) {
                pirate = this.moveShip(pirate.position, position);
                this.emitter.emit('changePiratesPosition', pirate);
            }
            else {
                pirate.previousPosition = pirate.position;
                pirate.position = position;
                if (!piece.opened) {
                    piece.opened = true;
                    this.emitter.emit('sendMapPieces', piece.public);
                    this.openedPieces.push(piece.public);
                }
            }
            if (!piece.walkingWithCoin) {
                var pirateResources = this.haveResource(pirate);     //utopit
                if (pirateResources.coin) {
                    this.exileResource('coin');
                    this.emitter.emit('checkGameStatus');
                }
                if (pirateResources.gold) {
                    this.exileResource('gold');
                    this.emitter.emit('checkGameStatus');
                }
            }


            if (piece.subType != 'peace' && this.anotherPiratesHere(position)) {
                var pirates = this.getPiratesByPosition(position);
                if (pirates.length > 0) {
                    for (i = 0; i < pirates.length; i += 1 ) {
                        if (pirates[i].userId != pirate.userId) {
                            this.pirateHasBeenKilledByOtherPirate(pirates[i]);
                        }
                    }
                }
            }

            if (pirate.previousPosition) {
                var previousPiece = this.getPiecesByPosition(pirate.previousPosition);
                if (previousPiece.subType == 'airplan') {
                    previousPiece.type = 'land';
                }

            }

            if (this.isTrap(piece)) {
                var kindOfTrap = piece.subType;
                if (this.traps.hasOwnProperty(kindOfTrap)) {
                    this.traps[kindOfTrap](pirate);
                }
                this.endOfTurn();
            } else {
                if (this.pieceTypesActions.hasOwnProperty(piece.type)) {
                        this.pieceTypesActions[piece.type](pirate);
                } else {
                    this.emitter.emit('changePiratesPosition', pirate);
                    this.nextStep();
                }
            }
        }

    };


    this.haveResource = function (element) {
        var data = {
                coin: false,
                rum: false,
                gold: false
            };
        var resources;

        if (element.is === 'pirate') {
            resources = element.resources;

        }

        else if (element.is === 'piece') {
            resources = element.public.resources;
        }

        for (var key in resources) {
            if (resources[key].length >= 1) {
                data[key] = true;
            }
        }

        return data;
    };


    this.getResourceStatusForTools = function (pirate) {
        var status = {};
        var piece = this.getPiecesByPosition(pirate.position);
        var piratesResStatus = this.haveResource(pirate);
        var piecesResStatus = this.haveResource(piece);
        var ship = this.getShipByUserId(pirate.userId);


        if (piratesResStatus.coin) {
            status.coin = 'drop';
            status.gold = 'nothing';
        } else if (piratesResStatus.gold == 1) {
            status.coin = 'nothing';
            status.gold = 'drop';
        } else if (piecesResStatus.rum) {
            status.rum = 'pickup';
        } else {
            if (pirate.penalties > 0 && this.haveResource(ship).rum) {
                if (this.haveResource(ship).rum) {
                    status.rum = 'drop'; // это значит использовать
                }
            } else {
                status.rum = 'nothing';
            }
            status.coin = (piecesResStatus.coin)? 'pickup' : 'nothing';
            status.gold = (piecesResStatus.gold)? 'pickup' : 'nothing';
        }
        return status;


    };

    this.exileResource = function (resourceType) {
        if (resourceType != 'rum') {
            this.exiledResources[resourceType].push(this.selectedPirate.resources[resourceType].pop());
        }
    };

    this.dropResource = function (resourceType, pirate) {
        if (resourceType != 'rum') {
            if (this.haveResource(pirate)[resourceType]) {
                var piece = this.getPiecesByPosition(pirate.position);
                piece.public.resources[resourceType].push(pirate.resources[resourceType].pop());
                this.emitter.emit('sendMapPieces', piece.public);
            }
        } else {
            var ship = this.getShipByUserId(pirate.userId);
            if (this.haveResource(ship)[resourceType]) {
                pirate.resources[resourceType].push(ship.public.resources[resourceType].pop());
                this.exileResource(resourceType);
                this.emitter.emit('sendMapPieces', ship.public);
                pirate.penalties = 0;
                var userName = this.getUserById(pirate.userId).name;
                this.emitter.emit('sendMessageToAll', {string: 'Йо-хо-хо! Игрок ' + userName + ' использует ром!', hideStatus: true});

            }
        }
        this.emitter.emit('changePiratesPosition', pirate);
    };

    this.pickupResource = function (resourceType, pirate) {
        var piece = this.getPiecesByPosition(pirate.position);
        if (resourceType != 'rum') {
            if (this.haveResource(piece)[resourceType] && !this.haveResource(pirate)[resourceType]) {
                pirate.resources[resourceType].push(piece.public.resources[resourceType].pop());
            }
            this.emitter.emit('sendMapPieces', piece.public);
            this.emitter.emit('changePiratesPosition', pirate);
        } else {
            var ship = this.getShipByUserId(pirate.userId);
            if (ship) {
                var rum = piece.public.resources[resourceType],
                    shipsResources = ship.public.resources[resourceType],
                    i,
                    rumCount = rum.length;

                for (i = 0; i < rumCount; i += 1) {
                    shipsResources.push(rum.pop())
                }
                this.emitter.emit('sendMapPieces', ship.public);
                this.emitter.emit('sendMapPieces', piece.public);
            }
        }


    };

    this.getPiratesShipPosition = function (pirate) {
        var ship;
        for (var i = 0; i < that.ships.length; i++) {
            if (that.ships[i].userId == pirate.userId) {
                ship = that.ships[i];
                break;
            }
        }

        return ship.public.position;
    };

    this.getPiratesVariations = function (pirate) {
        var piece = this.getPiecesByPosition(pirate.position);
        var variations = this.getVariationsByStepMethodRules(piece);
            variations = this.checkVariations(piece, variations);
        if (variations.length == 0 && !this.canSelectPirate) {

            this.endOfTurn();
        }
        this.currentVariation = variations;
    };

    this.getVariationsByRadius = function (pirate, radius) {
        var variations = [];
        var position = pirate.position;
        for (var y = position.y - radius; y <= position.y + radius; y++) {
            if (y >= 0 && y < this.mapSize) {
                for (var x = position.x - radius; x <= position.x + radius; x++) {
                    if (x >= 0 && x < this.mapSize) {
                        if (!(x == position.x && y == position.y))
                        {
                            variations.push({x: x, y: y});
                        }
                    }
                }
            }
        }
        return variations;
    };

    this.removeBackSlashDiagonal = function (pirate, variations) {
        var currentPosition = pirate.position;
        var newVariations = [];
        var f1 = currentPosition.x - currentPosition.y;

        for (var i = 0; i < variations.length; i++) {
            if (!(variations[i].x - variations[i].y == f1)) {
                newVariations.push(variations[i])
            }
        }
        return newVariations;
    };

    this.removeSlashDiagonal = function (pirate, variations) {
        var currentPosition = pirate.position;
        var newVariations = [];
        var f2 = (-1 * currentPosition.x) - currentPosition.y;
        for (var i = 0; i < variations.length; i++) {
            if (!((-1 * variations[i].x) - variations[i].y == f2)) {
                newVariations.push(variations[i])
            }
        }
        return newVariations;
    };

    this.removeDiagonals = function (pirate, variations) {
        var newVariations = this.removeBackSlashDiagonal(pirate, variations);
            newVariations = this.removeSlashDiagonal(pirate, newVariations);
        return newVariations;
    };

    this.removeVerticals = function (pirate, variations) {
        var currentPosition = pirate.position;
        var newVariations = [];
        for (var i = 0; i < variations.length; i++) {
            if (variations[i].y != currentPosition.y) {
                newVariations.push(variations[i])
            }
        }
        return newVariations
    };

    this.removeHorizontals = function (pirate, variations) {
        var currentPosition = pirate.position;
        var newVariations = [];
        for (var i = 0; i < variations.length; i++) {
            if (variations[i].x != currentPosition.x) {
                newVariations.push(variations[i])
            }
        }
        return newVariations
    };


    this.removePerpendiculars = function (pirate, variations) {
        var newVariations = this.removeVerticals(pirate, variations);
            newVariations = this.removeHorizontals(pirate, newVariations);
        return newVariations;
    };

    this.getNextValueByVector = function (current, previous) {
        var value;
        if (previous > current) value = current - 1;
        else if (previous < current) value = current + 1;
        else value = current;
        return value;
    };

    this.pirateHasBeenKilledByOtherPirate = function (pirate) {
        var shipPosition = this.getPiratesShipPosition(pirate),
            resources = this.haveResource(pirate);

        for (var key in resources) {
            if (resources[key]) {
                this.dropResource(key, pirate);
            }
        }

        pirate.previousPosition = pirate.position;
        pirate.position = shipPosition;
        this.emitter.emit('changePiratesPosition', pirate);
    };

    this.getOpenTunnelsVariations = function () {
        var variations = [],
            i,
            tunnels = this.tunnels,
            tunnelsLength = tunnels.length;

        for (i = 0; i < tunnelsLength; i += 1) {
            if (tunnels[i].opened) {
                variations.push(tunnels[i].public.position);
            }
        }

        return variations;
    };

    this.getActiveTunnelsVariations = function (pirate) {
        var variations = [],
            i,
            tunnels = this.getOpenTunnelsVariations(),
            tunnelsLength = tunnels.length;

            for (i = 0; i < tunnelsLength; i += 1) {
                if ((!this.positionEquality(pirate.position, tunnels[i])) &&
                    !this.anotherPiratesHere(tunnels[i])) {
                    variations.push(tunnels[i]);
                }
            }
        return variations;
    };


    this.stepMethodRules = {   // расчитываем для каждого типа
        common: {
            land: function (pirate) {
                pirate.lastPieceOptions.type = 'land';
                pirate.lastPieceOptions.subType = 'resource';
                return that.getVariationsByRadius(pirate, 1);
            },

            revival: function (pirate) {
                pirate.lastPieceOptions.type = 'revival';
                pirate.lastPieceOptions.subType = 'fortress';
                if (that.haveExiledPirates(pirate) && pirate.lastPieceOptions.type == 'revival') {
                    return [pirate.position]
                } else {
                    return that.getVariationsByRadius(pirate, 1);
                }

            },

            water: function (pirate) {
                pirate.lastPieceOptions.type = 'water';
                pirate.lastPieceOptions.subType = 'border';
                return that.getVariationsByRadius(pirate, 1);
            },

            ship: function (pirate) {
                var variations = that.getVariationsByRadius(pirate, 1);
                    variations = that.removeDiagonals(pirate, variations);
                var ship = that.getPiecesByPosition(pirate.position);
                    if (that.isType('water', variations)) {
                        variations = [pirate.previousPosition];
                    }

                pirate.lastPieceOptions.type = 'ship';
                pirate.lastPieceOptions.subType = 'ship';
                return variations;
            },

            'diagonal-arrow': function (pirate) {
                var rotate;
                var variations = that.getVariationsByRadius(pirate, 1);
                    variations = that.removePerpendiculars(pirate, variations);
                var piece = that.getPiecesByPosition(pirate.position);
                pirate.lastPieceOptions.type = 'arrow';
                pirate.lastPieceOptions.subType = 'four';

                if (piece.subType == 'one') {
                    rotate = piece.public.rotatable;
                    if (rotate % 2) {
                        variations = that.removeSlashDiagonal(pirate, variations);
                        (rotate + 0.1 > 2)? variations.splice(1,1) :  variations.splice(0,1);
                    }
                    else {
                        variations = that.removeBackSlashDiagonal(pirate, variations);
                        (rotate + 0.1 > 2)? variations.splice(0,1) :  variations.splice(1,1);
                    }
                    pirate.lastPieceOptions.subType = 'one';
                }

                if (piece.subType == 'two') {
                    rotate = piece.public.rotatable;
                    variations = (rotate % 2)? that.removeSlashDiagonal(pirate, variations) : that.removeBackSlashDiagonal(pirate, variations);
                    pirate.lastPieceOptions.subType = 'two';
                }

                return variations;
            },

            arrow: function (pirate) {
                var variations = that.getVariationsByRadius(pirate, 1);
                    variations = that.removeDiagonals(pirate, variations);
                var piece = that.getPiecesByPosition(pirate.position);
                pirate.lastPieceOptions.type = 'arrow';
                pirate.lastPieceOptions.subType = 'four';

                if (piece.subType == 'one') {
                    var rotate = piece.public.rotatable;
                    variations = (rotate % 2)? that.removeVerticals(pirate, variations) : that.removeHorizontals(pirate, variations);
                    (rotate + 0.1 > 2)? variations.splice(1,1) :  variations.splice(0,1);
                    pirate.lastPieceOptions.subType = 'one';
                }

                if (piece.subType == 'two') {
                    var rotate = piece.public.rotatable;
                    variations = (rotate % 2)? that.removeVerticals(pirate, variations) : that.removeHorizontals(pirate, variations);
                    pirate.lastPieceOptions.subType = 'two';
                }

                return variations;

            },

            horse: function (pirate) {
                var variations =  that.getVariationsByRadius(pirate, 2);
                variations = that.removeDiagonals(pirate, variations);
                pirate.lastPieceOptions.type = 'horse';
                return that.removePerpendiculars(pirate, variations);
            },

            tunnel: function (pirate) {
                var variations = [],
                    prevPiece = that.getPiecesByPosition(pirate.previousPosition),  // прошлая
                    piece = that.getPiecesByPosition(pirate.position);

                if (piece.type == 'tunnel' && prevPiece.type == 'tunnel') {
                    pirate.lastPieceOptions.type = 'tunnel';
                    variations = that.getVariationsByRadius(pirate, 1);
                } else {
                    pirate.lastPieceOptions.type = 'land';
                    variations = that.getActiveTunnelsVariations(pirate)
                }
                return variations;
            },

            gun: function (pirate) {
                var piece = that.getPiecesByPosition(pirate.position);
                var axis = (piece.public.rotatable % 2)? 'y' : 'x';
                var position = {};
                position.x = pirate.position.x;
                position.y = pirate.position.y;
                position[axis] = (piece.public.rotatable + 0.1 < 2)? that.mapSize - 1 : 0;
                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'gun';
                return [position];
            },

            x2: function (pirate) {
                var variations = [];
                if (pirate.lastPieceOptions.type == 'horse') {
                    variations = that.getVariationsByRadius(pirate, 2);
                    variations = that.removeDiagonals(pirate, variations);
                    pirate.lastPieceOptions.type = 'horse';
                    pirate.lastPieceOptions.subType = 'x2';
                    return that.removePerpendiculars(pirate, variations);
                } else {
                    var position = {};
                    var current = pirate.position;
                    var previous = pirate.previousPosition;
                    position.x = that.getNextValueByVector(current.x, previous.x);
                    position.y = that.getNextValueByVector(current.y, previous.y);
                    variations.push(position);
                    pirate.lastPieceOptions.type = 'special';
                    pirate.lastPieceOptions.subType = 'x2';
                    return variations;
                }

            }
        },

        special: {
            crocodile: function (pirate) {
                var variations = [];

                if (pirate.lastPieceOptions.subType == 'one') {
                    var piece = that.getPiecesByPosition(pirate.position);
                        piece.subType = 'cycle';
                        return [pirate.position];

                } else {
                    variations.push(pirate.previousPosition);
                }

                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'crocodile';
                return variations;

            },

            trap: function (pirate) {
                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'trap';
                if (pirate.penalties > 0) {
                    return [];
                } else {
                    return that.getVariationsByRadius(pirate, 1);
                }
            },

            labyrinth: function (pirate) {
                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'labyrinth';
                if (pirate.penalties > 0) {
                    return [pirate.position];
                } else {
                    return that.getVariationsByRadius(pirate, 1);
                }
            },

            airplan: function (pirate) {
                //var piece = that.getPiecesByPosition(pirate.position);
                var variations = that.getVariationsByRadius(pirate, that.mapSize);
                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'airplan';
                return variations;
            },

            airballoon: function (pirate) {
                pirate.lastPieceOptions.type = 'special';
                pirate.lastPieceOptions.subType = 'airballon';
                return [that.getPiratesShipPosition(pirate)];
            }

        }

    };

    this.endOfTurn = function () {
        if(!this.gameOver) {
            this.emitter.emit('changeTurn');
            this.canSelectPirate = true;
        }
    };

    this.nextStep = function () {
        this.canSelectPirate = false;
        this.emitter.emit('nextStep');
    };


    this.pieceTypesCompatibilityRules = {
        'land': {
            'water': false
        },

        'water': {
            'land': false,
            'arrow': false,
            'diagonal-arrow': false,
            'special': false,
            'horse': false,
            'tunnel': false
        },

        'horse': {
            'water': false
        },

        'ship': {
            'ship': false
        },

        'tunnel' : {
            'water': false
        },

        special: {
            water: false
        },

        'revival': {
            'water': false
        }

    };

    this.anotherPiratesHere = function (position) {
        var pirates = this.getPiratesByPosition(position);
        var i;
        if (pirates.length > 0) {
            for (i = 0; i < pirates.length; i += 1) {
                if (pirates[i].userId != this.selectedPirate.userId) {
                    return true
                }
            }
        } else {
            return false
        }
    };


    this.positionEquality = function (position1, position2) {
        return (position1.x == position2.x && position1.y == position2.y);
    };

    this.getVariationsByStepMethodRules = function (piece) {  // собрать возможности
        var variations;

        if (this.stepMethodRules.common.hasOwnProperty(piece.type)) {
            variations = this.stepMethodRules.common[piece.type](this.selectedPirate);
        }

        else if (this.stepMethodRules.special.hasOwnProperty(piece.subType)) {
            variations = this.stepMethodRules.special[piece.subType](this.selectedPirate);
        }

        else {
            variations = [];
        }

        return variations;
    };

    this.checkTypeCompatibility = function (currentPiece, pieces) {
        var variations = [];
        for (var i = 0; i < pieces.length; i++) {
            var bool = (!this.pieceTypesCompatibilityRules.hasOwnProperty(currentPiece.type))? true : !this.pieceTypesCompatibilityRules[currentPiece.type].hasOwnProperty(pieces[i].type);
            if (pieces[i].subType == 'fortress' && bool) {
                bool = (!this.anotherPiratesHere(pieces[i].public.position));
            }
            if (bool) {  //доп проверка
                    variations.push(pieces[i].public.position);
            }
        }
        return variations;
    };


    this.canDrownCoinFrom = function (piece, variationPiece) {
        var result = false;
        var canList = {
            type: {
                'arrow': true,
                'diagonal-arrow': true
            },

            subType: {
                'x2': true,
                'gun': true
            }
        };

        if (variationPiece.type == 'water') {
            if (piece.type == 'special') {
                result = canList.subType.hasOwnProperty(piece.subType);
            }
            else {
                result = canList.type.hasOwnProperty(piece.type);
            }
        }
        return result;
    };

    this.checkVariations = function (piece, variations) {
        var variationsPieces = [];
        var pirateResources = this.haveResource(this.selectedPirate);
        var pirateHaveResource = (pirateResources.coin || pirateResources.gold);
        var i;
        if (pirateHaveResource) {
            for ( i = 0; i < variations.length; i++) {
                var variationPiece = this.getPiecesByPosition(variations[i]);
                    if (((variationPiece.opened && variationPiece.walkingWithCoin) || this.canDrownCoinFrom(piece, variationPiece)) && !this.anotherPiratesHere(variationPiece.public.position)) {  //смотрим куда можно ходить с ними
                        variationsPieces.push(variationPiece);
                    }
            }
            if (variationsPieces.length == 0) {
                if (pirateResources.coin) {this.dropResource('coin', this.selectedPirate)}
                if (pirateResources.gold) {this.dropResource('gold', this.selectedPirate)}
                pirateHaveResource = false;
            }

        }
        if (!pirateHaveResource) {
            for ( i = 0; i < variations.length; i++) {
                variationsPieces.push(this.getPiecesByPosition(variations[i]));
            }
        }


        return this.checkTypeCompatibility(piece, variationsPieces);
    }
};


exports.map = Map;
