


var Room = require('./room-model').room;
var EventEmitter = require('events').EventEmitter;
var RoomsCollector = function (io) {
    var that = this;
    this.emitter = new EventEmitter();
    this.length = 0;
    this.rooms = {};

    this.addRoom = function () {
        var id = this.generateRoomID();
        var room = new Room(id, this.emitter);
            room.href = '/room?id=' + id;
        this.rooms[id] = room;
        this.length += 1;
        return room
    };

    this.generateRoomID = (function(){
        var id = 0;
        return function() {
            return id++ ;
        };
    })();

    this.addUserInRoom = function (user) {
        var room = this.getRoomById(user.roomId);
        if (room) {
            room.join(user);
        }
    };

    this.removeUserFromRoom = function (user) {
        if (user) {
            var room = this.getRoomById(user.roomId);
            if (room) {
                room.leave(user);
            }
        }
    };

    this.removeRoomById = function (id) {
        delete this.rooms[id];
    };



    this.getRoomById = function (id) {
        if (this.rooms.hasOwnProperty(id)) {
            return this.rooms[id];
        }
        else {
            return false;
        }
    };

    this.getOpenedRooms = function () {
        var rooms = [],
            thisRooms = this.rooms,
            room;

        if (this.length > 0) {
            for (var key in thisRooms) {
                if (thisRooms.hasOwnProperty(key)) {
                    if (thisRooms[key].open) {
                        room = thisRooms[key];
                        rooms.push({
                            id: room.id,
                            href: room.href,
                            usersCount: room.usersCount
                        })
                    }

                }
            }
        }
        return rooms;
    };

    this.initialize = function () {
        this.emitter.on('room:isEmpty', function (id) {
            that.removeRoomById(id);
        });
    };

    this.initialize();
};

exports.roomsCollector = RoomsCollector;

