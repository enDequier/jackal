

var form;
$('document').ready(function () {
     form = {
        $loginButton: $('#login'),
        $registrationButton: $('#registration'),
        $startText: $('.start-text'),
        $loginForm: $('.log-in'),
        $registrationForm: $('.reg-in'),
        $error: $('.error'),

        checkMail: function (emailString) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(emailString);
        },

        showRegistrationForm: function ($el) {
            console.log($el);
            this.$startText.hide();
            this.$registrationForm.show();
            this.$loginButton.hide();
            $el.off('click');
            this.setEventsOnRegistrationForm($el, this)
        },

        checkUserRegistrationData: function ($login, $nickname, $password, $password2, $el, that) {
            var data = {
                login: $login.val(),
                nickname: $nickname.val(),
                password: $password.val(),
                password2: $password2.val()
                },
                bool = true;

                console.log(data);

            if (bool) {
                if (data.login.length < 5) {
                    this.$error.text('длина Эл.Почты не меньше 5 символов').show();
                    bool = false;
                }
            }

            if (bool) {
                if (!this.checkMail(data.login)) {
                    this.$error.text('Некорректный адресс почты').show();
                    bool = false;
                }
            }

            if (bool) {
                if (data.nickname.length < 2 || data.nickname.length > 12) {
                    this.$error.text('Длина псевдонима 2-12 символов').show();
                    bool = false;
                }
            }

            if (bool) {
                if (data.password.length > 8) {
                    if (data.password != data.password2) {
                        this.$error.text('Пароли не совпадают').show();
                    }
                } else {
                    that.$error.text('Длина пароля не меньше 8 символов').show();
                    bool = false;
                }
            }

            if (bool) {
                this.$error.hide();
                $el.off('click');
                $password2.off('keyup');
                this.sendRegistrationData(data);
            }
        },

        sendRegistrationData: function (data) {
            var that = this;
            $.ajax({
                url: '/registration',
                method: "post",
                data: data,
                dataType: 'JSON'
            }).done(function(data) {
                if (data.status) {
                    window.location.replace(window.location.origin);
                } else {
                    that.$error.text(data.message).show();
                    that.setEventsOnRegistrationForm(that.$registrationButton, that)
                }
            })
        },

        setEventsOnRegistrationForm: function ($el, that) {
            var $login = this.$registrationForm.find('input[name="login"]'),
                $nickname = this.$registrationForm.find('input[name="nickname"]'),
                $password = this.$registrationForm.find('input[name="password"]'),
                $password2 = this.$registrationForm.find('input[name="password2"]');


            $el.on('click', function () {
                that.checkUserRegistrationData($login, $nickname, $password, $password2, $el, that);

            });

            $password2.on('keyup', function (key) {
                if (key.keyCode == 13) {
                    that.checkUserRegistrationData($login, $nickname, $password, $password2, $el, that)
                }
            });
        },



        sendUserLoginData: function (data) {
            var that = this;
            $.ajax({
                url: "/login",
                method: "post",
                data: data,
                dataType: 'JSON'
            }).done(function(data) {
                if (data.status) {
                    //console.log('logined')
                    window.location.replace(window.location.origin);
                } else {
                    //console.log('not logined')
                    that.$error.text('неправильный логин или пароль').show();
                    that.setEventsOnLoginForm(that.$loginButton, that);
                }
            })
        },

        checkUserLoginData: function ($login, $password, $el, that) {
            var data = {login: $login.val(), password: $password.val()};
            if (data.login.length > 0 && data.password.length > 0) {
                that.$error.hide();
                $el.off('click');
                $password.off('keyup');
                that.sendUserLoginData(data);
            } else {
                that.$error.text('заполните поля').show();
            }
        },

        setEventsOnLoginForm: function ($el, that) {
            var $login = this.$loginForm.find('input[name="login"]'),
                $password = this.$loginForm.find('input[name="password"]');


            $el.on('click', function () {
                that.checkUserLoginData($login, $password,  $el, that);
            });

            $password.on('keyup', function (key) {
                if (key.keyCode == 13) {
                    that.checkUserLoginData($login, $password, $el, that);
                }
            });
        },

        showLoginForm: function ($el) {
            console.log($el);
            this.$startText.hide();
            this.$loginForm.show();
            this.$registrationButton.hide();
            $el.off('click');
            this.setEventsOnLoginForm($el, this)
        },

        init: function () {
            var that = this;
            this.$loginButton.on('click', function () {
                that.showLoginForm($(this));
            });

            this.$registrationButton.on('click', function () {
                console.log('clicked regis');
                that.showRegistrationForm($(this));
            });

        }

    };




    form.init();

});