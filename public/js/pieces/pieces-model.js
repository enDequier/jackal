var PiecesModel = Backbone.Model.extend({
    defaults: {
        pieceId: 0,
        position: {
            x: undefined,
            y: undefined
        },
        rotatable: 0,
        resources: {
            rum: [],
            coin: [],
            gold: []
        },
        visited: 0
    }

});